from django.db import models

# Create your models here.

def get_overall_payment_amount():
    agg = Payment.objects.aggregate(models.Sum("amount"))
    return agg["amount__sum"]

def who_is_next():
    blame_user = None
    minimum = 99999
    for user in User.objects.all():
        if user.get_saldo() < minimum:
            minimum = user.get_saldo()
            blame_user = user
    return blame_user


class User(models.Model):
    name = models.CharField(max_length=200)


    def get_payment_amount(self):
        agg = Payment.objects.filter(von=self).aggregate(models.Sum("amount"))
        print("User payment", agg)
        if agg["amount__sum"]==None:
            return 0
        else:
            return agg["amount__sum"]

    def get_debt_amount(self):
        sum = 0
        for payment in Payment.objects.filter(payment_for_user__user=self):
            sum += payment.amount / len(payment.payment_for_user_set.all())
        return sum

    def get_saldo(self):
        return self.get_payment_amount() - get_overall_payment_amount()/len(User.objects.all())


    def __str__(self):
        return self.name

class Payment(models.Model):
    amount = models.FloatField()
    von = models.ForeignKey(User, on_delete=models.CASCADE)
    fuer = models.ManyToManyField(User, related_name="pay_User")
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    description = models.CharField(max_length=200)


    def __str__(self):
        return self.description


#class Payment_User(models.Model):
    #user = models.ForeignKey(Payment, on_delete=models.CASCADE)
    #payment = models.ForeignKey(User, on_delete=models.CASCADE)





