from django.shortcuts import render
from .models import User, Payment
from django.http import HttpResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from django.urls import reverse
from . import models
from django.db.models import Avg, Count, Min, Sum, Max

# Create your views here.
def index(request):
    myusers = User.objects.all()
    mypayments = Payment.objects.all()


    summe = Payment.objects.aggregate(Sum("amount"))

    users = User.objects.all()
#    payments = Payment.objects.all()
#    overall_payment_amount = models.get_overall_payment_amount()
    who_is_next = models.who_is_next()


    context = {
        "myusers": myusers,
        "mypayments": mypayments,
        "summe": summe["amount__sum"],

        "who_is_next" : who_is_next,
    }

    return render(request, 'letspay/index.html', context)

def user_add(request):
    name = request.POST["name"]
    User.objects.create(name = name)

    return HttpResponseRedirect(reverse('letspay:index'))


def payment_to_user(request):
    amount = request.POST["amount"]
    user_id = request.POST.get("name")
    von = User.objects.get(pk=user_id)
    fuer_id = request.POST.getlist("adressat")
    print(fuer_id)
    fuer = User.objects.filter(pk__in=fuer_id)
    print(fuer)
    datetime = request.POST["date"]
    description = request.POST["text"]
    print(amount, von, fuer, datetime, description)

    payment = Payment.objects.create(amount = amount,
                        von = von,
                           #fuer wird später gemacht, da das set nicht direkt angelegt werden kann
                        date_time = datetime,
                        description =description)

    payment.fuer.add(*fuer)
    #* dröselt die Liste auf. erkennt jedes Element
    #alt.:payment.fuer.set
    payment.save()
    return HttpResponseRedirect(reverse('letspay:index'))


def user_details(request, user_id):
    user_id = user_id
    user_name = User.objects.get(pk=user_id)
    #payment_fuer = Payment.objects.all()
    context = {
        "user_id": user_id,
        "user_name": user_name,
        "mypayments": Payment.objects.filter(von=user_name),

    }
    return render(request, "letspay/details.html", context)



def salden(request):
    users = User.objects.all()
    context = {
        "users": users,
    }
    return render(request, "letspay/salden.html", context)

def zahlung(request):
    myusers = User.objects.all()
    mypayments = Payment.objects.all()
    context = {
        "myusers": myusers,
        "mypayments": mypayments,}
    return render(request, "letspay/zahlung.html", context)







#Wichtig: User sind IDs. Als IDs in der DB abgelegt.
#An den Browser wird die ID abgeschickt als Post: Mit Auswählen der Checkbox wird die ID übermittelt.

#1.Wir haben User-IDs
#2.Wir holen die User aus der Datenbank
#3.Payment wird neu angelegt
#4.Schleife über User-ID:
#User holen
#neues Payment_for_user anlegen
#FK setzen
