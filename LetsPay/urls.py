"""paying URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from . import views

app_name = "letspay"

urlpatterns = [
    path('', views.index, name='index'),
    path('user_add', views.user_add, name='user_add'),
    path("user_details/<int:user_id>/", views.user_details, name="user_details"),
    path("payment_to_user", views.payment_to_user, name="payment_to_user"),
    path("salden", views.salden, name="salden"),
    path("zahlung", views.zahlung, name="zahlung"),
]
