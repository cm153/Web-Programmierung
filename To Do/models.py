import datetime
from django.db import models
from django.utils import timezone

class Task(models.Model):
    text = models.CharField(max_length=200)
    created = models.DateTimeField('pub_date')
    deadline = models.DateTimeField("deadline")
    status_choices = [
        ("D", "Done"),
        ("T", "Todo"),
        ("C", "Canceled")
    ]
    status = models.CharField(max_length=200,choices=status_choices, default="T")
    priority = models.CharField(max_length=200)

    def __str__(self):
        return self.text
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


