from django.http import Http404

from django.http import HttpResponse, HttpResponseRedirect

from django.urls import reverse

from django.shortcuts import get_object_or_404

from .models import Shop, Item
from django.shortcuts import render

#def index(request):
    #return render(request, 'shoppinglist/index.html')

def index(request):
    myitems = Item.objects.all()
    context = {'myitems': myitems}
    return render(request, 'shoppinglist/index.html', context)

def shops(request):
    myshops = Shop.objects.all()
    context = {"myshops" : myshops}
    return render(request, "shoppinglist/shops.html", context)

def neu_item(request):
    name = request.POST.get('name', False)
    amount = request.POST.get("amount", False)
    status = request.POST.get("status", False)
    shop_id = request.POST.get("shop", False)
    shop = Shop.objects.get(pk = shop_id)
    print(name, amount, status, shop)

    Item.objects.create(name = name,
                        amount = amount,
                        status = status,
                        shop = shop)

    return HttpResponseRedirect(reverse('shoppinglist:index'))


def neu_shop(request):
    name = request.POST.get('name', False)
    Shop.objects.create(name = name)
    return HttpResponseRedirect(reverse('shoppinglist:shops'))

